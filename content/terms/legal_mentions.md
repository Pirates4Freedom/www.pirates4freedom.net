+++
title = "legal mentions"
+++

pirates4freedom.net (referenced as p4f after) is edited by Hugo Mechiche also known as UncleReaton.

## Host
p4f is self-hosted

## Code of Conduct
By registering on p4f, you certify that you have read and accepted [p4f's code of conduct.](/terms/coc/)

## What information do we collect?
Basic account information: When you register on p4f, you are asked to enter a username and a password. You may also enter additional profile information such as a link and biography. The username, biography are always listed publicly.  
IPs and other metadata: When you log in, we record the IP address you log in from, as well as the exact time you log in. IPs are stored for up to 12 months.

## What do we use your information for?
Any of the information we collect from you may be used in the following ways:

To provide the core functionality of p4f. You can only interact with other people's content and hop your own content when you are logged in.  
To aid moderation of the website, for example comparing your IP address with other known ones to determine ban evasion or other violations.

## How do we protect your information?
We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information. Among other things, your browser session is secured with SSL, and your password is hashed using a strong one-way algorithm.

## What is our data retention policy?
We will make a good faith effort to:

*   Retain the IP addresses associated with registered users no more than 12 months.
*   Retain deleted posts and comments no more than 12 months.
*   Retain the e-mail addresses used to get an inscription key no more than 1 month.

You may irreversibly delete your account at any time.

## Do we disclose any information to outside parties?
We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our site, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety.

## Change to our privacy policy

If we decide to change our privacy policy, we will display those changes on this page. This document is CC-BY-SA. It was last updated on July 30, 2021.  

This document is based on [koyu.space's terms](https://koyu.space/terms)
