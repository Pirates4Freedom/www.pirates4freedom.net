+++
title = "Code of Conduct"
+++

## I - Respect
You must respect your peers. p4f is a safe place and must remain so. No bigotry, no racism, no hateful speech of any sort is allowed here.

## II - French Laws
Every post and every comment must respect the French laws.

## III - No conspiracy theorist
It is forbidden to post conspiracy theories or anything close to them. Keep a critical mind and don't fall into the easy way.
